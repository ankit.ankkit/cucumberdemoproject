package com.toll.objectRepo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	@FindBy(id = "reg_login_email")
	public static WebElement emaildIdfield;


	@FindBy(id = "reg_login_btn")
	public static WebElement SignInButton;
	
	
	
	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}

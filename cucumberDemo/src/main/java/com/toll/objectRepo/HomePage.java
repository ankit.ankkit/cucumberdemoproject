package com.toll.objectRepo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

	@FindBy(xpath = "//a[@class='pp_auth_btn pp_login_btn']")
	public static WebElement loginButton;

	
	@FindBy(xpath = "//a[@class='pp_auth_btn pp_register_btn']")
	public static WebElement registerButton;
	
	
	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}

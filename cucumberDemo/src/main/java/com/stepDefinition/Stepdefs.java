package com.stepDefinition;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.toll.objectRepo.HomePage;
import com.toll.objectRepo.LoginPage;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;

public class Stepdefs {
	

	public static WebDriver driver;
	public HomePage H1;
	public LoginPage L1;
	

	@Given("^Launching Chrome Browser$")
	public void launchingBrowser() {
			
		H1 = new HomePage(driver);
		L1 = new LoginPage(driver);
	}

	
	@Given("click on Register Button")
	public void click_on_register_button() throws InterruptedException {
		H1.registerButton.click();
		Thread.sleep(3000);
	}
	

	@Given("click on Login button")
	public void click_on_Login_button() throws InterruptedException {
		H1.loginButton.click();
		Thread.sleep(3000);
	}
	
	
	@Given("enter credentials")
	public void enter_credentials() {
	 L1.emaildIdfield.sendKeys("test.test@gmail.com");
	 L1.SignInButton.click();
	}
	
	@After
    public void afterScenario(){
        driver.quit();
    }
	
	
	 @Before
	    public void beforeScenario(){
		 String driverPath = "C:\\Softwares\\chromedriver_win32\\";
			System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
			driver = new ChromeDriver();
			driver.navigate().to("http://prep.edgecanvas.com/home-page/");
	    }
	
}
